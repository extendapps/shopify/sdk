/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import * as log from 'N/log';
import {Order} from '@extendapps/shopifyapi/Objects/Order';
import {ShopifyEventDetails} from './Interfaces/Shopify_EventDetails';
import {ShopifyTopicHandler} from './Interfaces/Shopify_TopicHandler';

export let cancelled: ShopifyTopicHandler.OrderHandler.handler = (eventDetails: ShopifyEventDetails, order: Order) => {
    log.audit('order - cancelled (CUSTOM)', order);
};

export let create: ShopifyTopicHandler.OrderHandler.handler = (eventDetails: ShopifyEventDetails, order: Order) => {
    log.audit('order - create (CUSTOM)', order);
};

// tslint:disable-next-line:variable-name
export let _delete: ShopifyTopicHandler.OrderHandler.handler = (eventDetails: ShopifyEventDetails, order: Order) => {
    log.audit('order - delete (CUSTOM)', order);
};

export let fulfilled: ShopifyTopicHandler.OrderHandler.handler = (eventDetails: ShopifyEventDetails, order: Order) => {
    log.audit('order - fulfilled (CUSTOM)', order);
};

export let paid: ShopifyTopicHandler.OrderHandler.handler = (eventDetails: ShopifyEventDetails, order: Order) => {
    log.audit('order - paid (CUSTOM)', order);
};

// tslint:disable-next-line:variable-name
export let partially_fulfilled: ShopifyTopicHandler.OrderHandler.handler = (eventDetails: ShopifyEventDetails, order: Order) => {
    log.audit('order - partially_fulfilled (CUSTOM)', order);
};

export let updated: ShopifyTopicHandler.OrderHandler.handler = (eventDetails: ShopifyEventDetails, order: Order) => {
    log.audit('order - updated (CUSTOM)', order);
};
