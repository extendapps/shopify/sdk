/**
 * @copyright 2020 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType UserEventScript
 */

import {EntryPoints} from 'N/types';
import * as log from 'N/log';
import {ShopifyApp} from '../Interfaces/ShopifyApp';
// @ts-ignore
import {SHOPIFY} from '/.bundle/283386/com.extendapps.shopify/Shopify';

export let afterSubmit: EntryPoints.UserEvent.afterSubmit = (context: EntryPoints.UserEvent.afterSubmitContext) => {
    switch (context.type) {
        case context.UserEventType.SHIP:
            const shopify: ShopifyApp = new SHOPIFY('your domain');

            shopify.API.createFulfillment({
                order_id: 12345,
                fulfillment: {
                    location_id: 21567754,
                    tracking_number: '123456789',
                    tracking_urls: [
                        'https://shipping.xyz/track.php?num=123456789',
                        'https://anothershipper.corp/track.php?code=abc'
                    ],
                    'notify_customer': false
                },
                Created: shopifyFulfillment => {
                    log.audit('createFulfillment - Created', shopifyFulfillment);
                }
            });
            break;

        default:
            break;
    }
};
