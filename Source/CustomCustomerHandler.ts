/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import * as log from 'N/log';
import {Customer} from '@extendapps/shopifyapi/Objects/Customer';
import {ShopifyEventDetails} from './Interfaces/Shopify_EventDetails';
import {ShopifyTopicHandler} from './Interfaces/Shopify_TopicHandler';

export let create: ShopifyTopicHandler.CustomerHandler.handler = (eventDetails: ShopifyEventDetails, customer: Customer) => {
    log.audit('customer - create (CUSTOM)', customer);
};

// tslint:disable-next-line:variable-name
export let _delete: ShopifyTopicHandler.CustomerHandler.handler = (eventDetails: ShopifyEventDetails, customer: Customer) => {
    log.audit('customer - delete (CUSTOM)', customer);
};

export let disable: ShopifyTopicHandler.CustomerHandler.handler = (eventDetails: ShopifyEventDetails, customer: Customer) => {
    log.audit('customer - disable (CUSTOM)', customer);
};

export let enable: ShopifyTopicHandler.CustomerHandler.handler = (eventDetails: ShopifyEventDetails, customer: Customer) => {
    log.audit('customer - enable (CUSTOM)', customer);
};

export let update: ShopifyTopicHandler.CustomerHandler.handler = (eventDetails: ShopifyEventDetails, customer: Customer) => {
    log.audit('customer - update (CUSTOM)', customer);
};
