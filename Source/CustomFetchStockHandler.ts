/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

export let fetchStock: FetchStockFunction = (maxRetries: number, shop: string, sku: string, timestamp: number) => {
    const skuInventoryCount: any = {};
    skuInventoryCount[sku] = 1000;

    // Example Return Payload
    return skuInventoryCount;
};

export type FetchStockFunction = (maxRetries: number, shop: string, sku: string, timestamp: number) => any;
