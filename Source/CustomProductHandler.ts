/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import * as log from 'N/log';
import {Product} from '@extendapps/shopifyapi/Objects/Product';
import {ShopifyEventDetails} from './Interfaces/Shopify_EventDetails';
import {ShopifyTopicHandler} from './Interfaces/Shopify_TopicHandler';

export let create: ShopifyTopicHandler.ProductHandler.handler = (eventDetails: ShopifyEventDetails, product: Product) => {
    log.audit('product - create (CUSTOM)', product);
};

// tslint:disable-next-line:variable-name
export let _delete: ShopifyTopicHandler.ProductHandler.handler = (eventDetails: ShopifyEventDetails, product: Product) => {
    log.audit('product - delete (CUSTOM)', product);
};

export let update: ShopifyTopicHandler.ProductHandler.handler = (eventDetails: ShopifyEventDetails, product: Product) => {
    log.audit('product - update (CUSTOM)', product);
};
