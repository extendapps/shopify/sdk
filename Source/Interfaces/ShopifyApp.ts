/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {Order} from '@extendapps/shopifyapi/Objects/Order';
import {Customer} from '@extendapps/shopifyapi/Objects/Customer';
import {Product} from '@extendapps/shopifyapi/Objects/Product';
import {ShopifyAPI} from '../shopifyapi';

export interface ShopifyApp {
    readonly API: ShopifyAPI;

    decorateOrder(order: Order): void;

    decorateCustomer(customer: Customer): void;

    decorateProduct(product: Product): void;

    getWebHookAddress(): string;
}
