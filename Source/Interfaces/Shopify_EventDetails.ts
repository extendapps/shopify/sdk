/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface ShopifyEventDetails {
    id: number;
    topic: {
        name: string;
        id: number;
    };
    storeFront: {
        name: string;
        id: number;
    };
    status: {
        name: string;
        id: number;
    };
    payLoad: string;
    notes: string;
}
