/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface GetNetSuiteItemIdsOptions {
    storeFrontId?: number;
    product_ids: number[];

    found(foundIds: number[]): void;
}
