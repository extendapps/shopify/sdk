/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {ShopifyEventDetails} from './Shopify_EventDetails';
import {Customer} from '@extendapps/shopifyapi/Objects/Customer';
import {Order} from '@extendapps/shopifyapi/Objects/Order';
import {Product} from '@extendapps/shopifyapi/Objects/Product';
import {Refund} from '@extendapps/shopifyapi/Objects/Refund';

export namespace ShopifyTopicHandler {
    export namespace CustomerHandler {
        // noinspection JSUnusedGlobalSymbols
        export type handler = (
            eventDetails: ShopifyEventDetails,
            customer: Customer
        ) => void;
    }
    export namespace OrderHandler {
        // noinspection JSUnusedGlobalSymbols
        export type handler = (
            eventDetails: ShopifyEventDetails,
            order: Order
        ) => void;
    }
    export namespace ProductHandler {
        // noinspection JSUnusedGlobalSymbols
        export type handler = (
            eventDetails: ShopifyEventDetails,
            product: Product
        ) => void;
    }
    export namespace RefundHandler {
        // noinspection JSUnusedGlobalSymbols
        export type handler = (
            eventDetails: ShopifyEventDetails,
            refund: Refund
        ) => void;
    }
}
